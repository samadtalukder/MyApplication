package com.samadtalukder.calculatorapp;

import android.app.Activity;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import net.objecthunter.exp4j.Expression;
import net.objecthunter.exp4j.ExpressionBuilder;

import java.util.regex.Pattern;

public class MainActivity extends Activity {

    //ID for all numeric button
//    private int[] numericButtons={R.id.btn_one,R.id.btn_two,R.id.btn_three,R.id.btn_four,R.id.btn_five,R.id.btn_six,R.id.btn_seven,R.id.btn_eight,R.id.btn_nine,R.id.btn_zero};

    //ID for all the Operator Buttons
//    private int[] operatorButtons={R.id.btn_plus,R.id.btn_minus,R.id.btn_multipole,R.id.btn_division,R.id.btn_persent};

    //TextView Used display for output
    private TextView txtDisplay;
    //
    private String Display="";
    //
    private String currentOperator="";
    //
    private String result="";


    //Represent the lastly pressed key is numeric
//    private boolean lastNumeric;

    //If True,do not allow to add another DOT
//    private boolean Dot;

    //
//    private boolean stateError;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //Find the TextView
        this.txtDisplay = (TextView) findViewById(R.id.output_text);
        txtDisplay.setText(Display);
    }
    private void updateScreen(){
        txtDisplay.setText(Display);
    }

    public void onClickNumber(View v)
    {
        if (result !="")
        {
            clear();
            updateScreen();
        }
        Button b=(Button) v;
        Display +=b.getText();
        updateScreen();

    }
    private boolean isOperator(char op)
    {
        switch (op)
        {
            case '+':
            case '-':
            case '*':
            case '/':
                return true;
            default:return false;
        }

    }

    public void onClickOperator(View v)
    {
        if (Display =="")return;
        Button b=(Button) v;
        if (result!="")
        {
            String _Display=result;
            clear();
            Display=_Display;
        }
        if (currentOperator !="")
        {
            Log.d("Calc MSG",""+Display.charAt(txtDisplay.length()-1));

            if (isOperator(Display.charAt(Display.length()-1)))
            {
                Display=Display.replace(Display.charAt(Display.length()-1),b.getText().charAt(0));
                updateScreen();
                return;
            }
            else {
                getResult();
                Display=result;
                result="";
            }
            currentOperator=b.getText().toString();

        }

        Display +=b.getText();
        currentOperator=b.getText().toString();
        updateScreen();
    }



    private void clear() {
        Display="";
        currentOperator="";
        result="";
    }
    public void onClickClear(View v){
        clear();
        updateScreen();

    }

    private double operate(String a,String b,String op)
    {
        switch (op)
        {
            case "+":
                return Double.valueOf(a) + Double.valueOf(b);
            case "-":
                return Double.valueOf(a) - Double.valueOf(b);
            case "*":
                return Double.valueOf(a) * Double.valueOf(b);
            case "/":
                try {
                    return Double.valueOf(a) / Double.valueOf(b);

                }
                catch (Exception e)
                {
                    Log.d("Calc MSG","");
                }
                default:return -1;

        }

    }

    private boolean getResult() {
        if (currentOperator =="") return false;
        String[] operation=Display.split(Pattern.quote(currentOperator));

        if (operation.length < 2) return false;
        result=String.valueOf(operate(operation[0],operation[1],currentOperator));

        return true;

    }

    public void onClickEqual(View v)
    {
        if (Display =="") return;
        if (!getResult()) return;
        txtDisplay.setText(Display+ "=" +String.valueOf(result));

    }
}

    //Find and set OnclickListener to Numeric Buttons
//        setNumericOnClicklistener();

        //Find and set OnClickListener to Operator Buttons
//        setOperatorOnClickListener();


//    }

    ///Find and set OnclickListener to Numeric Buttons
//    private void setNumericOnClicklistener() {
//        View.OnClickListener listener= new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//
//                //set text clicked button
//                Button btn=(Button) v;
//
//                if (stateError)
//                {
//                    //If current state is error ,replace the error message
//                    txtOutput.setText(btn.getText());
//                    stateError=false;
//                }
//                else {
//                    //if not already there is a valid expression
//                    txtOutput.append(btn.getText());
//                }
//                lastNumeric=true;
//
//            }
//        };

        //Assign the listener to all the numeric buttons
//        for (int id:numericButtons)
//        {
//            findViewById(id).setOnClickListener(listener);
//        }
//
//    }

    //Find and set OnClickListener to operator buttons, equal button and decimal point button

//    private void setOperatorOnClickListener() {
//
//        View.OnClickListener listener=new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//
//                //if the current state is error do not the append the operator
//                //if the last input is number only ,append the operator
//
//                if (lastNumeric && !stateError)
//                {
//                    Button btn=(Button) v;
//                    txtOutput.append(btn.getText());
//                    lastNumeric=false;
//                    Dot=false;
//                }
//
//            }
//        };
        //Assign the listener to all the opearator buttons

//        for (int id:operatorButtons)
//        {
//            findViewById(id).setOnClickListener(listener);
//        }

//        findViewById(R.id.btn_plus).setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                if (lastNumeric && !stateError && !Dot)
//                {
//                    txtOutput.append("+");
//                    lastNumeric=false;
//                    Dot=true;
//                }
//            }
//        });

        //Decimal Point
//        findViewById(R.id.btn_dot).setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                if (lastNumeric && !stateError && !Dot)
//                {
//                    txtOutput.append(".");
//                    lastNumeric=false;
//                    Dot=true;
//                }
//            }
//        });

        //Equal Button
//        findViewById(R.id.btn_equal).setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                onEqual();
//            }
//
//
//        });
//    }

    //Logic to calculation

//    private void onEqual() {
//
//        if (lastNumeric && !stateError)
//        {
//            //Read the expression
//            String txt=txtOutput.getText().toString();
//
//            //create an expression (from exp4j library)
//            Expression expression=new ExpressionBuilder(txt).build();
//            try{
//                //calculate the result and show display
//                double result=expression.evaluate();
//                txtOutput.setText(Double.toString(result));
//                Dot=true;
//
//            }
//            catch (ArithmeticException ex)
//            {
//                //display an error message
//                txtOutput.setText("Error");
//                stateError=true;
//                lastNumeric=false;
//            }
//        }
//    }


